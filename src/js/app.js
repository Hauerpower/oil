import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';



(function ($) {


	// $(document).foundation();

	$(document).ready(function () {

		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit();






		// /////////////////////////////dots!!!
		// $('.shadow-ball').click(function (e) {
		// 	e.preventDefault();
		// 	let element_id = $(this).attr('data-show-text');
		// 	let target = ".anim-text-" + element_id;
		// 	let position = $(this).position();
		// 	let tarHeight = $(target).height();
		// 	if ($(".anim-text:visible").not($(target)).length) {
		// 		$(".anim-text").hide();
		// 	}
		// 	$(target).css("top", position.top - tarHeight - 60);
		// 	$(target).css("left", position.left - 100);
		// 	$(target).toggle();
		// });


		//////////////////job-toggle
		$(".offer").click(function () {


			let id_faq = $(this).attr('data-show-offer');
			let target = '.offer-text-' + id_faq;
			if ($(".offer-text:visible").not($(target)).length) {
				$(".offer-text").hide();
				// $(".offer-text").slideUp();

				$(this).siblings().removeClass('offer-added-class rotate');
			} 

				$(this).toggleClass('offer-added-class rotate');
				$(target).slideToggle();

			


		});

		$(".offer").click(function () {
			$('html,body').animate({
				scrollTop: $(this).offset().top
				// scrollTop: $(this).offset().top - 100
			}, 'slow');
		});

//menu-sticky
			$(window).scroll(function () {
				if ($(window).width() >= 980) {
					if ($(this).scrollTop() > 1) {
						$('#navbar').addClass("sticky");
						$('.header-main').removeClass("stretch-left grid-container-left");
						$('.logo-box-box').addClass("medium-6 large-6");
						$('.logo-box-box').removeClass("medium-5 large-3");
			
					} else {
						$('#navbar').removeClass("sticky");
						$('.header-main').addClass("stretch-left grid-container-left");
						$('.logo-box-box').removeClass("medium-6 large-6");
						$('.logo-box-box').addClass("medium-5 large-3");
					}

				}
		});


		
//sąd-picker
		$('.sad-picker-1').addClass("sad-add-class");
		$('.sad-picker').click(function (e) {
			e.preventDefault()

			let element_id = $(this).attr('data-show-sad');
			let target = ".sad-cell-" + element_id;
			$('.sad-cell-box').hide();
			$(target).show();
			$('.sad-picker').removeClass("sad-add-class");
			$(this).addClass("sad-add-class");
		})

		//news-menu-small
		$('.news-menu-1').addClass("news-add-class");
		$('.news-menu').click(function (e) {
			e.preventDefault()

			let element_id = $(this).attr('data-show-news');
			let target = ".anim-news-" + element_id;
			$('.anim-news').hide();
			$(target).show();
			$('.news-menu').removeClass("news-add-class");
			$(this).addClass("news-add-class");
		})

		/////////special-menu-toggle

		$('.title-box').click(function (e) {
			console.log('cliced');
			e.preventDefault()
			let element_id = $(this).attr('data-show-submenu');
			let target = ".submenu-" + element_id;
			if ($(".special-submenu:visible").not($(target)).length) {
				$(".special-submenu").slideUp();
				$('.title-box').removeClass('rotate add-opacity');
			}
			$(target).slideToggle();
			$(this).toggleClass('rotate add-opacity');
		});

		///special-submenu

			$('body').on('click', '.struktura', function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-struktura');
			let target = ".struktura-item-" + element_id;
			$('.struktura-item').hide();
			$('.struktura').removeClass('struktura-item-added-class');
			$(target).show();
			$(this).addClass("struktura-item-added-class");

		});

		//nie mam pojęcia, czemu to nie działa, spojrzę po uroplie


		$('body').on('click', '.search-loop', function (e) {
			$('.search-bar-wraper ').slideToggle();
		});
	


		





	
	});








})(jQuery);